/**
 *
 * @licstart  The following is the entire license notice for the
 *  JavaScript code in this page.
 *
 * Copyright (C) 2015  Tux Solbakk
 *
 *
 * The JavaScript code in this page is free software: you can
 * redistribute it and/or modify it under the terms of the GNU
 * General Public License (GNU GPL) as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.  The code is distributed WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
 *
 * As additional permission under GNU GPL version 3 section 7, you
 * may distribute non-source (e.g., minimized or compacted) forms of
 * that code without the copy of the GNU GPL normally required by
 * section 4, provided you include this license notice and a URL
 * through which recipients can access the Corresponding Source.
 *
 * @licend  The above is the entire license notice
 * for the JavaScript code in this page.
 *
 */

var jqgfTable = document.registerElement('jqgf-table');
var jqgfSlider = document.registerElement('jqgf-slider');
var jqgfPane = document.registerElement('jqgf-pane');
var jqgfMain = document.registerElement('jqgf-main');
var jqgfSlideHandler = document.registerElement('jqgf-slide-handler');

(function($) {
	$.jqgf = function (params) {
		$('jqgf-table').jqgf(params);
	};
})(jQuery);


(function($) {
	var defaults = {
		width: [],
		open: [],
		minWidth: 50,
		defaultWidth: 100,
		storeState: true
	};

	var settings;

	$.fn.jqgf = function (options, param) {
		var table = $('jqgf-table');
		var main = table.children('jqgf-main');

		var hide = function (num) {
			var thisPane = $('jqgf-pane:eq(' + num + ')');

			if (thisPane.is(':visible')) {

				thisPane.hide();
				settings.open[num] = false;

				mainSubtract = 0;
				table.children(':visible').each(function () {
					if (!$(this).is(main)) {
						mainSubtract += $(this).outerWidth();
					}
				});

				main.css('width', 'calc(100% - ' + mainSubtract + 'px)');

				if (settings.storeState == true) {
					localStorage.setItem('jqgf', JSON.stringify({
						width: settings.width,
						open: settings.open
					}));
				}
			}
		};

		var show = function (num) {
			var thisPane = $('jqgf-pane:eq(' + num + ')');

			if (thisPane.is(':hidden')) {

				thisPane.show();
				settings.open[num] = true;

				mainSubtract = 0;
				table.children(':visible').each(function () {
					if (!$(this).is(main)) {
						mainSubtract += $(this).outerWidth();
					}
				});

				main.css('width', 'calc(100% - ' + mainSubtract + 'px)');

				if (settings.storeState == true) {
					localStorage.setItem('jqgf', JSON.stringify({
						width: settings.width,
						open: settings.open
					}));
				}
			}
		};

		if (options && typeof(options) == 'string') {

			if (options == 'hide') {
				hide(param);
			}
			if (options == 'show') {
				show(param);
			}
			return;
		}


		settings = $.extend(true, {}, defaults, options);

		if (settings.storeState == true) {
			if (items = localStorage.getItem('jqgf')) {
				items = JSON.parse(items);
				if (items.width) {
					settings.width = items.width;
				}
				if (items.open) {
					settings.open = items.open;
				}
			}
		}

		var panes = 0;
		table.children('jqgf-pane').each(function () {
			if (settings.width[panes] === undefined) {
				$(this).width(settings.width[panes] = settings.defaultWidth);
			}
			if (settings.open[panes] === undefined) {
				settings.open[panes] = true;
			}
			$(this).width(settings.width[panes]);
			if (settings.open[panes] == false) {
				hide(panes);
			}
			panes++;
		});


		mainSubtract = 0;
		table.children(':visible').each(function () {
			var that = $(this);
			if (!that.is(main)) {
				mainSubtract += that.outerWidth();
			}
		});
		slidersWidth = 0;
		table.children('jqgf-slider').each(function () {
			slidersWidth += $(this).outerWidth();
		});

		main.css('minWidth', settings.minWidth);
		main.css('width', 'calc(100% - ' + mainSubtract + 'px)');

		table.on('mousedown', 'jqgf-slide-handler', sliderDown = function (e) {
			e.stopPropagation();
		});

		table.on('click', 'jqgf-slide-handler', sliderDown = function (e) {
			var parent = $(this).parent();

			var pos = parent.prevAll('jqgf-main').length;
			if (pos == 0) {
				if (parent.prev().is(':visible')) {
					hide(parent.index('jqgf-slider'));
				} else {
					show(parent.index('jqgf-slider'));
				}
			} else {
				if (parent.next().is(':visible')) {
					hide(parent.index('jqgf-slider'));
				} else {
					show(parent.index('jqgf-slider'));
				}
			}
		});

		table.on('mousedown', 'jqgf-slider', sliderDown = function (e) {
			if (e.button == 0) {
				var ePageX = e.pageX;
				var first = false;
				var pos = $(this).prevAll('jqgf-slider').length;
				if ($(this).prevAll('jqgf-main').length == 0) {
					first = true;
				}
				if (first == true) {
					var that = $(this).prev();
				} else {
					var that = $(this).next();
				}
				var thatWidth = that.width();
				var mainWidth = main.width();

				if (that.is(':visible')) {
					$(document).on('mousemove', sliderMove = function (e) {
						var moved = ePageX - e.pageX;
						if (first == true) {
							var newAsideWidth = thatWidth - moved;
						} else {
							var newAsideWidth = thatWidth + moved;
						}

						if (newAsideWidth <= settings.minWidth) {
							newAsideWidth = settings.minWidth;
						}

						var tmpSize = settings.width;

						tmpSize[pos] = newAsideWidth;
						var totalWidth = 0;
						for (var i = 0; i < panes; i++) {
							if ($('jqgf-pane').eq(i).is(':visible')) {
								totalWidth += tmpSize[i];
							}
						}
						totalWidth += slidersWidth;
						if ($(document).width() <= totalWidth + settings.minWidth) {
							return;
						}

						settings.width[pos] = newAsideWidth;
						that.css('width', newAsideWidth + 'px');
						main.css('width', 'calc(100% - ' + totalWidth + 'px');

						if (settings.storeState == true) {
							localStorage.setItem('jqgf', JSON.stringify({
								width: settings.width,
								open: settings.open
							}));
						}
					});

					$(document).one('mouseup', sliderUp = function () {
						$(document).off('mousemove', sliderMove);
					});
				}
			}
		});

	};
})(jQuery);
